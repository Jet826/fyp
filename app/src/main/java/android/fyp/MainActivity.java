package android.fyp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

/**
 * Created by jet_0 on 4/7/2017.
 */

public class MainActivity extends AppCompatActivity{

    SharedPreferences sharedpreferences;
    public static final String  PrefsName = "MyPrefs";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        sharedpreferences = getSharedPreferences(PrefsName , Context.MODE_PRIVATE);
        boolean isLoggedIn= sharedpreferences.getBoolean("Status", false);

        if (!isLoggedIn)
        {
            Intent intent = new Intent(this, LogIn.class);
            startActivity(intent);
        }
    }

    public void onLogout(View view) {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
        finish();
        startActivity(getIntent());
    }
}