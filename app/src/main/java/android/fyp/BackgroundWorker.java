package android.fyp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;

import com.fasterxml.jackson.core.JsonParser;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import static android.fyp.MainActivity.PrefsName;

/**
 * Created by jet_0 on 14/6/2017.
 */

public class BackgroundWorker extends AsyncTask<String,Void,String> {

    Context context;
    AlertDialog alertDialog;
    SharedPreferences sharedpreferences;

    BackgroundWorker(Context ctx){
        context = ctx;
    }
    public String type;

    @Override
    protected String doInBackground(String... params) {
        type = params[0];
        String result = "";

        switch(type){
            case "login":
                String username = params[1];
                String password = params[2];
                JSONObject json = new JSONObject();
                try {
                    json.put("username", username);
                    json.put("password", password);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return postConnection("login", json);


        }
        return result;
    }
    private String postConnection(String apiCall, JSONObject json ){
        String baseURL = "http://192.168.0.145:3000/";
        HttpURLConnection urlConn = null;
        String result = "";
        try {
            String loginURL = baseURL + apiCall;
            URL url = new URL(loginURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

            byte[] outputInBytes = json.toString().getBytes("UTF-8");
            OutputStream os = conn.getOutputStream();
            os.write( outputInBytes );
            os.close();


            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            while ((output = br.readLine()) != null) {
                result += output;
                System.out.println(output);
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
        System.out.println(result);
        return result;
    }

    @Override
    protected void onPreExecute() {
        alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Login Status");

    }

    @Override
    protected void onPostExecute(String result) {
        System.out.println("HERE " + result);
        switch(type){
            case "login":
                //PARSE RESULT
                JSONObject json = null;
                int statusCode = 0;

                try {
                     json = new JSONObject(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                     statusCode = json.getInt("status");
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                if(statusCode == 1){
                    try {
                        JSONObject response = json.getJSONObject("response");
                        String username = response.getString("name");
                        alertDialog.setMessage("Welcome " + username);
                        alertDialog.show();

                        sharedpreferences = context.getSharedPreferences(PrefsName , Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedpreferences.edit();
                        editor.putString("UserName" , username);
                        editor.putBoolean("Status", true);
                        editor.commit();

                        Intent intent = new Intent(this.context, MainActivity.class);
                        context.startActivity(intent);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if ( statusCode == -1 ){
                    try {
                        String message = json.getString("message");
                        alertDialog.setMessage(message);
                        alertDialog.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }


        }



    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

}
